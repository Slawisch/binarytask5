﻿using BinaryTask3.DAL.EF;
using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace BinaryTask3.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private ProjectRepository _projectRepository;
        private TaskRepository _taskRepository;
        private TeamRepository _teamRepository;
        private UserRepository _userRepository;

        private readonly ProjectsContext _projectsContext;

        public UnitOfWork(ProjectsContext dbContext)
        {
            _projectsContext = dbContext;
        }

        public IRepository<Project> Projects => _projectRepository ??= new ProjectRepository(_projectsContext);

        public IRepository<TaskEntity> Tasks => _taskRepository ??= new TaskRepository(_projectsContext);

        public IRepository<Team> Teams => _teamRepository ??= new TeamRepository(_projectsContext);

        public IRepository<User> Users => _userRepository ??= new UserRepository(_projectsContext);

        public void SaveChanges()
        {
            _projectsContext.SaveChanges();
        }

        public void SaveChangesAsync()
        {
            _projectsContext.SaveChangesAsync();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using BinaryTask3.DAL.EF;
using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;

namespace BinaryTask3.DAL.Repositories
{
    public class ProjectRepository : IRepository<Project>
    {
        private readonly ProjectsContext _db;

        public ProjectRepository(ProjectsContext db)
        {
            this._db = db;
        }

        public IEnumerable<Project> Read()
        {
            return _db.Projects;
        }

        public Project Read(int id)
        {
            if (_db.Projects.Find(id) != null)
                return _db.Projects.Find(id);
            throw new ArgumentException($"Project with id({id}) not found");
        }

        public void Update(int id, Project project)
        {
            Delete(id);
            Create(project);
        }

        public void Create(Project project)
        {
            if(_db.Projects.Find(project.Id) == null)
                _db.Projects.Add(project);
            else
                throw new ArgumentException($"Project with id({project.Id}) already exists");
        }

        public void Delete(int id)
        {
            var projectForDelete = _db.Projects.Find(id);
            if (projectForDelete != null)
                _db.Projects.Remove(projectForDelete);
            else
                throw new ArgumentException($"Project with id({id}) not found");
        }
    }
}

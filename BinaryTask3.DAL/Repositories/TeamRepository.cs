﻿using System;
using System.Collections.Generic;
using BinaryTask3.DAL.EF;
using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;

namespace BinaryTask3.DAL.Repositories
{
    class TeamRepository : IRepository<Team>
    {
        private readonly ProjectsContext _db;

        public TeamRepository(ProjectsContext db)
        {
            this._db = db;
        }

        public IEnumerable<Team> Read()
        {
            return _db.Teams;
        }

        public Team Read(int id)
        {
            if (_db.Teams.Find(id) != null)
                return _db.Teams.Find(id);
            throw new ArgumentException($"Team with id({id}) not found");
        }

        public void Update(int id, Team team)
        {
            Delete(id);
            Create(team);
        }

        public void Create(Team team)
        {
            if (_db.Teams.Find(team.Id) == null)
                _db.Teams.Add(team);
            else
                throw new ArgumentException($"Team with id({team.Id}) already exists");
        }

        public void Delete(int id)
        {
            var teamForDelete = _db.Teams.Find(id);
            if (teamForDelete != null)
                _db.Teams.Remove(teamForDelete);
            else
                throw new ArgumentException($"Team with id({id}) not found");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;

namespace BinaryTask3.DAL.EF
{
    public class ProjectsContext : DbContext
    {
        public ProjectsContext(DbContextOptions<ProjectsContext> options) : base(options)
        { }

        public DbSet<Project> Projects { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<TaskEntity> Tasks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var Users = new List<User>()
            {
                new User()
                {
                    Id = 1,
                    BirthDay = DateTime.Now.AddYears(-22), 
                    Email = "EmailText", 
                    FirstName = "UserFirstName1", 
                    LastName = "UserLastName1",
                    TeamId = 1
                },
                new User()
                {
                    Id = 2,
                    BirthDay = DateTime.Now.AddYears(-30),
                    Email = "Email@gmail.com",
                    FirstName = "UserFirstName2",
                    LastName = "UserLastName2",
                    RegisteredAt = DateTime.Now.AddDays(-15),
                    TeamId = 1
                }
            };

            var Teams = new List<Team>()
            {
                new Team()
                {
                    CreatedAt = DateTime.Now,
                    Id = 1,
                    Name = "Team1"
                }
            };

            var Tasks = new List<TaskEntity>()
            {
                new TaskEntity()
                {
                    Id = 1,
                    CreatedAtDate = DateTime.Now,
                    PerformerId = 2,
                    ProjectId = 1,
                    State = TaskState.State0,
                    Name = "Task1Name",
                    Description = "Task1Description",
                    FinishedAtDate = DateTime.Now
                },
                new TaskEntity()
                {
                    Id = 2,
                    CreatedAtDate = DateTime.Now,
                    PerformerId = 1,
                    ProjectId = 2,
                    State = TaskState.State0,
                    Name = "Task2Name",
                    Description = "Task2Description",
                },
            };

            var Projects = new List<Project>()
            {
                new Project()
                {
                    Id = 1,
                    AuthorId = 1,
                    CreatedAtDate = DateTime.Now,
                    DeadlineDate = DateTime.Now.AddDays(5),
                    TeamId = 1,
                    Name = "Project1",
                    Description = "DescriptionForProject1"
                },
                new Project()
                {
                    Id = 2,
                    AuthorId = 1,
                    CreatedAtDate = DateTime.Now,
                    DeadlineDate = DateTime.Now.AddDays(5),
                    TeamId = 1,
                    Name = "Project2",
                    Description = "DescriptionForProject2"
                }
            };

            modelBuilder.Entity<User>().HasData(Users);
            modelBuilder.Entity<Team>().HasData(Teams);
            modelBuilder.Entity<TaskEntity>().HasData(Tasks);
            modelBuilder.Entity<Project>().HasData(Projects);

            base.OnModelCreating(modelBuilder);
        }
        
    }
}

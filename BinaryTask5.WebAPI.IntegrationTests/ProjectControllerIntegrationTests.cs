﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using BinaryTask3.Common.DTOs;
using BinaryTask3.WebAPI;
using Newtonsoft.Json;
using Xunit;
using Xunit.Abstractions;

namespace BinaryTask5.WebAPI.IntegrationTests
{
    public class ProjectControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public ProjectControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async void CreateProject_WhenNewProject_ThenStatusCodeOK()
        {
            var project = new ProjectDTO()
            {
                AuthorId = 1, 
                CreatedAt = new DateTime(2010, 2, 18),
                Deadline = new DateTime(2022, 9, 1),
                Name = "ProjectForIntegrationTest",
                TeamId = 1
            };
            var jsonInString = JsonConvert.SerializeObject(project);

            var httpResponse = await _client.PostAsync("api/Projects", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }

        [Fact]
        public async void CreateProject_WhenExistingProject_ThenStatusCodeConflict()
        {
            var project = new ProjectDTO()
            {
                Id = 5,
                AuthorId = 1,
                CreatedAt = new DateTime(2010, 2, 18),
                Deadline = new DateTime(2022, 9, 1),
                Name = "ProjectForIntegrationTest",
                TeamId = 1
            };
            var jsonInString = JsonConvert.SerializeObject(project);

            await _client.PostAsync("api/Projects", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            var httpResponse = await _client.PostAsync("api/Projects", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.Conflict, httpResponse.StatusCode);
        }
    }
}

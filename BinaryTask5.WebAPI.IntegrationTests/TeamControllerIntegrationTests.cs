﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BinaryTask3.Common.DTOs;
using BinaryTask3.WebAPI;
using Newtonsoft.Json;
using Xunit;
using Xunit.Abstractions;

namespace BinaryTask5.WebAPI.IntegrationTests
{
    public class TeamControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public TeamControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async void CreateTeam_WhenNewTeam_ThenStatusCodeOK()
        {
            var team = new TeamDTO()
            { 
                Name = "TeamForTest",
                CreatedAt = DateTime.Now,
            };
            var jsonInString = JsonConvert.SerializeObject(team);

            var httpResponse = await _client.PostAsync("api/Teams", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }

        [Fact]
        public async void CreateTeam_WhenExistingTeam_ThenStatusCodeConflict()
        {
            var team = new TeamDTO()
            {
                Id = 5,
                Name = "TeamForTest",
                CreatedAt = DateTime.Now,
            };
            var jsonInString = JsonConvert.SerializeObject(team);

            await _client.PostAsync("api/Teams", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            var httpResponse = await _client.PostAsync("api/Teams", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.Conflict, httpResponse.StatusCode);
        }

        [Fact]
        public async void GetTeamById1_WhenNoTeams_ThenNotFound()
        {
            var httpResponse = await _client.GetAsync("api/Teams/1");

            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }

        [Fact]
        public async void GetTeamById1_WhenTeamExists_ThenOKAndEqualTeamName()
        {
            var team = new TeamDTO()
            {
                Id = 5,
                Name = "TeamForTest",
                CreatedAt = DateTime.Now,
            };
            var jsonInString = JsonConvert.SerializeObject(team);

            await _client.PostAsync("api/Teams", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            var httpResponse = await _client.GetAsync("api/Teams/5");
            var teamResult = JsonConvert.DeserializeObject<TeamDTO>(httpResponse.Content.ReadAsStringAsync().Result);

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Equal("TeamForTest", teamResult.Name);
        }

    }
}

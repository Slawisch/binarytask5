﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using BinaryTask3.Common.DTOs;
using BinaryTask3.WebAPI;
using Newtonsoft.Json;
using Xunit;
using Xunit.Abstractions;

namespace BinaryTask5.WebAPI.IntegrationTests
{
    public class UserControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public UserControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async void DeleteUser_WhenNoUsers_ThenNotFound()
        {

            var httpResponse = await _client.DeleteAsync("api/Users/1");

            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }

        [Fact]
        public async void DeleteUser_WhenUserExists_ThenOK()
        {
            var testUser = new UserDTO()
            {
                Id = 1,
                BirthDay = new DateTime(2000, 1, 1),
                FirstName = "Bill",
                LastName = "Gates",
                RegisteredAt = DateTime.Now,
                TeamId = 1
            };

            var jsonInString = JsonConvert.SerializeObject(testUser);

            await _client.PostAsync("api/Users", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            var httpResponse = await _client.DeleteAsync("api/Users/1");

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BinaryTask3.Common.DTOs;
using BinaryTask3.DAL.Entities;
using BinaryTask3.WebAPI;
using Newtonsoft.Json;
using Xunit;
using Xunit.Abstractions;

namespace BinaryTask5.WebAPI.IntegrationTests
{
    public class CustomControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly ITestOutputHelper _testOutputHelper;
        private readonly HttpClient _client;

        public CustomControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory, ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
            _client = factory.CreateClient();
        }
        
        [Fact]
        public async void GetUnfinishedTasks_WhenNoTasks_ThenNoContent()
        {
            var user = new UserDTO()
            {
                Id = 10,
                BirthDay = new DateTime(2000, 1, 1),
                FirstName = "Bill",
                LastName = "Gates",
                RegisteredAt = DateTime.Now,
                TeamId = 1
            };

            var jsonInString = JsonConvert.SerializeObject(user);

            await _client.PostAsync("api/Users", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            var httpResponse = await _client.GetAsync("api/Custom/unfinished_Tasks/10");

            Assert.Equal(HttpStatusCode.NoContent, httpResponse.StatusCode);
        }

        [Fact]
        public async void GetUnfinishedTasks_When1FinishedAnd1Unfinished_ThenCount1()
        {
            var user = new UserDTO()
            {
                Id = 10,
                BirthDay = new DateTime(2000, 1, 1),
                FirstName = "Bill",
                LastName = "Gates",
                RegisteredAt = DateTime.Now,
                TeamId = 1
            };

            var tasks = new List<TaskDTO>()
            {
                new TaskDTO()
                {
                    Name = "TaskForBill1",
                    CreatedAt = new DateTime(2018, 5, 10),
                    PerformerId = 10,
                    ProjectId = 1
                },
                new TaskDTO()
                {
                    Name = "TaskForBill2",
                    CreatedAt = new DateTime(2017, 2, 1),
                    PerformerId = 10,
                    ProjectId = 1,
                    FinishedAt = DateTime.Today
                }
            };

            await _client.PostAsync("api/Users", new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json"));
            await _client.PostAsync("api/Tasks", new StringContent(JsonConvert.SerializeObject(tasks[0]), Encoding.UTF8, "application/json"));
            await _client.PostAsync("api/Tasks", new StringContent(JsonConvert.SerializeObject(tasks[1]), Encoding.UTF8, "application/json"));

            var httpResponse = await _client.GetAsync("api/Custom/unfinished_Tasks/10");

            var resultString = httpResponse.Content.ReadAsStringAsync().Result;

            var result = JsonConvert.DeserializeObject<IEnumerable<TaskDTO>>(JsonConvert.DeserializeObject(resultString).ToString());

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Single(result);
        }
    }
}

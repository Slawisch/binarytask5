﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BinaryTask3.Common.DTOs;
using BinaryTask3.WebAPI;
using Newtonsoft.Json;
using Xunit;
using Xunit.Abstractions;

namespace BinaryTask5.WebAPI.IntegrationTests
{
    public class TaskControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public TaskControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async void DeleteTask_WhenNoTasks_ThenNotFound()
        {
            var httpResponse = await _client.DeleteAsync("api/Tasks/1");

            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }

        [Fact]
        public async void DeleteTask_WhenTaskExists_ThenOK()
        {
            var task = new TaskDTO()
            {
                Id = 1,
                CreatedAt = DateTime.Now,
                PerformerId = 1,
                ProjectId = 1,
                State = 0,
                Name = "TestTask"
            };

            var jsonInString = JsonConvert.SerializeObject(task);

            await _client.PostAsync("api/Tasks", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            var httpResponse = await _client.DeleteAsync("api/Tasks/1");

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }
    }
}

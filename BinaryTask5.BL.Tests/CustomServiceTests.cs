using BinaryTask3.BLL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BinaryTask3.BLL.MappingProfiles;
using BinaryTask3.DAL.EF;
using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Xunit;
using Xunit.Abstractions;

namespace BinaryTask5.BL.Tests
{
    public class CustomServiceTests
    {
        private readonly ITestOutputHelper _testOutputHelper;
        private readonly IMapper _mapper;
        private readonly DbContextOptions<ProjectsContext> _dbContextOptions;

        public CustomServiceTests(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
            _dbContextOptions = new DbContextOptionsBuilder<ProjectsContext>().UseInMemoryDatabase(
                "Server=(localdb)\\mssqllocaldb;Database=ProjectsDatabaseCustomServiceTest;Trusted_Connection=True;").Options;
            
            _mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
            }));

            Seed();
        }

        private void Seed()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                var users = new List<User>()
                {
                    new User()
                    {
                        BirthDay = DateTime.Now.AddYears(-25),
                        Email = "EmailText",
                        FirstName = "Tom",
                        LastName = "Hunt",
                        TeamId = 1
                    },
                    new User()
                    {
                        BirthDay = DateTime.Now.AddYears(-15),
                        Email = "Email@gmail.com",
                        FirstName = "Gordon",
                        LastName = "Cartwright",
                        RegisteredAt = DateTime.Now.AddDays(-29),
                        TeamId = 1
                    }
                };

                var testTeam =
                    new Team()
                    {
                        CreatedAt = DateTime.Now,
                        Name = "DreamTeam"
                    };

                var testTasks = new List<TaskEntity>()
                {
                    new TaskEntity()
                    {
                        CreatedAtDate = DateTime.Now,
                        PerformerId = 2,
                        ProjectId = 1,
                        State = TaskState.State0,
                        Name = "JustTask",
                        Description = "DescriptionForJustTask",
                        FinishedAtDate = DateTime.Now
                    },
                    new TaskEntity()
                    {
                        CreatedAtDate = DateTime.Now,
                        PerformerId = 1,
                        ProjectId = 1,
                        State = TaskState.State0,
                        Name = "AwesomeTask",
                        Description = "Desc xD",
                        FinishedAtDate = DateTime.Now
                    }

                };

                var testProject =
                    new Project()
                    {
                        AuthorId = 1,
                        CreatedAtDate = DateTime.Now,
                        DeadlineDate = DateTime.Now.AddDays(5),
                        TeamId = 1,
                        Name = "ProjectD",
                        Description = "Lorem ipsum dolor sit amet"
                    };

                context.Projects.Add(testProject);
                context.Tasks.AddRange(testTasks);
                context.Teams.Add(testTeam);
                context.Users.AddRange(users);

                context.SaveChanges();
            }
        }

        [Fact]
        public void GetProjectTaskCountByUser_WhenGetBy1_ThenNotEmpty()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                var customService = new CustomService(new UnitOfWork(context), _mapper);
                var result = customService.GetProjectTaskCountByUser(1);

                Assert.NotEmpty(result);
            }
        }

        [Fact]
        public void GetProjectTaskCountByUser_WhenGetBy0_ThenEmpty()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                var customService = new CustomService(new UnitOfWork(context), _mapper);
                var result = customService.GetProjectTaskCountByUser(0);

                Assert.Empty(result);
            }
        }

        [Fact]
        public void GetTasksByUserLess45_WhenGetBy0_ThenEmpty()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                var customService = new CustomService(new UnitOfWork(context), _mapper);
                var result = customService.GetTasksByUserLess45(0);

                Assert.Empty(result);
            }
        }

        [Fact]
        public void GetTasksByUserLess45_WhenGetBy1_ThenNotEmpty()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                var customService = new CustomService(new UnitOfWork(context), _mapper);
                var result = customService.GetTasksByUserLess45(1);

                Assert.NotEmpty(result);
            }
        }

        [Fact] public void GetTasksByUserDone_WhenGetBy1_ThenAwesomeTaskAndOnlyOne()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                var customService = new CustomService(new UnitOfWork(context), _mapper);
                var result = customService.GetTasksByUserDone(1).ToArray();

                Assert.Single(result);
                Assert.Contains("AwesomeTask", result.Select(p => p.Item2));
            }
        }

        [Fact]
        public void GetTasksByUserDone_WhenGetBy0_ThenEmpty()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                var customService = new CustomService(new UnitOfWork(context), _mapper);
                var result = customService.GetTasksByUserDone(0);

                Assert.Empty(result);
            }
        }

        [Fact]
        public void GetTeamsWithUsersOlder10_When_ThenSingle()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                var customService = new CustomService(new UnitOfWork(context), _mapper);
                var result = customService.GetTeamsWithUsersOlder10();

                Assert.Single(result);
            }
        }

        [Fact]
        public void GetTeamsWithUsersOlder10_WhenOnlyUsers_ThenCount4()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                var customService = new CustomService(new UnitOfWork(context), _mapper);
                var result = customService.GetTeamsWithUsersOlder10();

                Assert.Equal(4, result.Values.SelectMany(i => i.Select(u => u.FirstName)).Count());
            }
        }

        [Fact]
        public void GetUsersWithTasks_WhenCall_ThenNotEmpty()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                var customService = new CustomService(new UnitOfWork(context), _mapper);
                var result = customService.GetUsersWithTasks();

                Assert.NotEmpty(result);
            }
        }

        [Fact]
        public void GetUserStruct_WhenById0_ThenInvalidOperationException()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                var customService = new CustomService(new UnitOfWork(context), _mapper);
                Assert.Throws<InvalidOperationException>(() => customService.GetUserStruct(0));
            }
        }

        [Fact]
        public void GetUserStruct_WhenById1_ThenNotEmpty()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                var customService = new CustomService(new UnitOfWork(context), _mapper);
                var result = customService.GetUserStruct(1);

                Assert.Equal("UserFirstName1", result.User.FirstName);
            }
        }

        [Fact]
        public void GetProjectStruct_WhenCall_ThenNotEmpty()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                var customService = new CustomService(new UnitOfWork(context), _mapper);
                var result = customService.GetProjectStruct();

                Assert.NotEmpty(result);
            }
        }

        [Fact]
        public void GetUnfinishedTasks_WhenNoUser_ThenArgumentException()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                var customService = new CustomService(new UnitOfWork(context), _mapper);

                Assert.Throws<ArgumentException>(() => customService.GetUnfinishedTasksByUserId(0));
            }
        }

        [Fact]
        public void GetUnfinishedTasks_When2Tasks_ThenCount2()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                var customService = new CustomService(new UnitOfWork(context), _mapper);

                context.Users.Add(new User()
                {
                    Id = 10,
                    BirthDay = new DateTime(2000, 1, 1),
                    FirstName = "Bill",
                    LastName = "Gates",
                    RegisteredAt = DateTime.Now,
                    TeamId = 1
                });

                context.Tasks.Add(new TaskEntity()
                {
                    Name = "TaskForBill1",
                    CreatedAtDate = new DateTime(2018,5,10),
                    PerformerId = 10,
                    ProjectId = 1,
                    State = TaskState.State3
                });

                context.Tasks.Add(new TaskEntity()
                {
                    Name = "TaskForBill2",
                    CreatedAtDate = new DateTime(2017, 2, 1),
                    PerformerId = 10,
                    ProjectId = 1,
                    State = TaskState.State2,
                    FinishedAtDate = DateTime.Today
                });

                context.Tasks.Add(new TaskEntity()
                {
                    Name = "TaskForBill3",
                    CreatedAtDate = new DateTime(2015, 10, 18),
                    PerformerId = 10,
                    ProjectId = 1,
                    State = TaskState.State3,
                    FinishedAtDate = null
                });

                context.SaveChanges();

                _testOutputHelper.WriteLine(JsonConvert.SerializeObject(context.Users.ToList()));

                var result = customService.GetUnfinishedTasksByUserId(10);

                Assert.Equal(2, result.Count());
            }
        }
    }
}



﻿using System;
using System.Linq;
using AutoMapper;
using BinaryTask3.BLL.MappingProfiles;
using BinaryTask3.BLL.Services;
using BinaryTask3.Common.DTOs;
using BinaryTask3.DAL.EF;
using BinaryTask3.DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace BinaryTask5.BL.Tests
{
    public class TaskServiceTests
    {
        private readonly IMapper _mapper;
        private readonly DbContextOptions<ProjectsContext> _dbContextOptions;

        public TaskServiceTests()
        {
            _dbContextOptions = new DbContextOptionsBuilder<ProjectsContext>().UseInMemoryDatabase(
                "Server=(localdb)\\mssqllocaldb;Database=ProjectsDatabaseTaskServiceTest;Trusted_Connection=True;").Options;

            _mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
            }));

            Seed();
        }

        private void Seed()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
            }
        }

        [Fact]
        public void UpdateTask_WhenCall_ThenOldTaskDoesNotExistAndNewExists()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                var taskService = new TaskService(new UnitOfWork(context), _mapper);
                taskService.UpdateTask(1, new TaskDTO() {Name = "TestTask"});

                Assert.Contains("TestTask", context.Tasks.Select(i => i.Name));
                Assert.DoesNotContain(1, context.Tasks.Select(i => i.Id));

            }
        }

        [Fact]
        public void UpdateTask_WhenCallById0_ThenArgumentException()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                var taskService = new TaskService(new UnitOfWork(context), _mapper);

                Assert.Throws<ArgumentException>(() => taskService.UpdateTask(0, new TaskDTO() {Name = "TestTask"}));
            }

        }

    }
}

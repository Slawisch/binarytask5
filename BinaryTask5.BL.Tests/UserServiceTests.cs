﻿using System;
using System.Linq;
using AutoMapper;
using BinaryTask3.BLL.Interfaces;
using BinaryTask3.BLL.MappingProfiles;
using BinaryTask3.BLL.Services;
using BinaryTask3.Common.DTOs;
using BinaryTask3.DAL.EF;
using BinaryTask3.DAL.Repositories;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace BinaryTask5.BL.Tests
{
    public class UserServiceTests
    {
        private readonly IMapper _mapper;
        private readonly DbContextOptions<ProjectsContext> _dbContextOptions;

        public UserServiceTests()
        {
            _dbContextOptions = new DbContextOptionsBuilder<ProjectsContext>().UseInMemoryDatabase(
                "Server=(localdb)\\mssqllocaldb;Database=ProjectsDatabaseUserServiceTest;Trusted_Connection=True;").Options;

            _mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
            }));

            Seed();
        }

        private void Seed()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                context.SaveChanges();
            }
        }

        [Fact]
        public void CreateUser_WhenCallTo_ThenCreationIsHappened()
        {
            var userService = A.Fake<IUserService>();
            userService.CreateUser(new UserDTO());

            A.CallTo(
                () => userService.CreateUser(A<UserDTO>._)).MustHaveHappenedOnceOrMore();
        }

        [Fact]
        public void CreateUser_WhenAlreadyExists_ThenArgumentException()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                var userService = new UserService(new UnitOfWork(context), _mapper);

                Assert.Throws<ArgumentException>(() => userService.CreateUser(new UserDTO() {Id = 1}));
            }

        }

        [Fact]
        public void CreateUser_WhenTeamId1_ThenTeamWithId1HasUser()
        {
            using (var context = new ProjectsContext(_dbContextOptions))
            {
                var userService = new UserService(new UnitOfWork(context), _mapper);

                userService.CreateUser(new UserDTO() {TeamId = 1, FirstName = "TestUserNameForTeam1"});

                Assert.Contains("TestUserNameForTeam1",
                    context.Users.Where(i => i.TeamId == 1).Select(u => u.FirstName));
            }

        }

    }
}

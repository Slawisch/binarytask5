﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using BinaryTask1.Models;
using Newtonsoft.Json;

namespace BinaryTask1.Services
{
    class LectureService
    {
        private readonly HttpService _httpService;

        public LectureService(HttpService service)
        {
            _httpService = service;
        }

        public void SendTestData()
        {
            _httpService.SendData();
        }

        public Dictionary<Project, int> GetProjectTaskCountByUser(int id)
        {
            string jsonData = _httpService.GetProjectTaskCountByUser(id);
            string jsonPretty = JsonConvert.DeserializeObject<string>(jsonData);
            var result = JsonConvert.DeserializeObject<IEnumerable<KeyValuePair<Project, int>>>(jsonPretty);
            return result.ToDictionary(i => i.Key, i => i.Value);
        }

        public IEnumerable<BinaryTask1.Models.Task> GetTasksByUserLess45(int id)
        {
            string jsonData = _httpService.GetTasksByUserLess45(id);
            var result = JsonConvert.DeserializeObject<IEnumerable<BinaryTask1.Models.Task>>(jsonData);
            return result ?? new List<Task>();
        }

        public IEnumerable<(int, string)> GetTasksByUserDone(int id)
        {
            string jsonData = _httpService.GetTasksByUserDone(id);
            var result = JsonConvert.DeserializeObject<IEnumerable<(int, string)>>(jsonData);
            return result ?? new List<(int, string)>();
        }

        public Dictionary<int, IEnumerable<User>> GetTeamsWithUsersOlder10()
        {
            string jsonData = _httpService.GetTeamsWithUsersOlder10();
            var result = JsonConvert.DeserializeObject<IEnumerable<KeyValuePair<int, IEnumerable<BinaryTask1.Models.User>>>>(jsonData);
            return result.ToDictionary(i => i.Key, i => i.Value);
        }

        public Dictionary<User, IEnumerable<BinaryTask1.Models.Task>> GetUsersWithTasks()
        {
            string jsonData = _httpService.GetUsersWithTasks();
            string jsonPretty = JsonConvert.DeserializeObject<string>(jsonData);
            var result = JsonConvert.DeserializeObject<IEnumerable<KeyValuePair<User, IEnumerable<BinaryTask1.Models.Task>>>>(jsonPretty);
            return result.ToDictionary(i => i.Key, i => i.Value);
        }

        public UserStruct GetUserStruct(int id)
        {
            string jsonData = _httpService.GetUserStruct(id);
            var result = JsonConvert.DeserializeObject<UserStruct>(jsonData);
            return result;
        }

        public IEnumerable<ProjectStruct> GetProjectStruct()
        {
            string jsonData = _httpService.GetProjectStruct();
            var result = JsonConvert.DeserializeObject<IEnumerable<ProjectStruct>>(jsonData);
            return result;
        }
    }
}

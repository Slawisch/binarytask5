﻿using System;
using System.Linq;
using BinaryTask3.BLL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace BinaryTask3.WebAPI.Controllers
{
    [ApiController]
    [Route("api/Custom")]
    public class CustomController : ControllerBase
    {
        private readonly ICustomService _customService;
        public CustomController(ICustomService customService)
        {
            _customService = customService;
        }

        [HttpGet("project_task_by_user/{id}")]
        public IActionResult GetProjectTaskCountByUser(int id)
        {
            try
            {
                return new JsonResult(JsonSerializer.Serialize(_customService.GetProjectTaskCountByUser(id).ToArray()));
            }
            catch
            {
                return new NotFoundResult();
            }
        }

        [HttpGet("tasks_by_user/{id}")]
        public IActionResult GetTasksByUserLess45(int id)
        {
            try
            {
                return new JsonResult(_customService.GetTasksByUserLess45(id));
            }
            catch
            {
                return new NotFoundResult();
            }
        }

        [HttpGet("tasks_by_user_done/{id}")]
        public IActionResult GetTasksByUserDone(int id)
        {
            try
            {
                return new JsonResult(JsonConvert.SerializeObject(_customService.GetTasksByUserDone(id)));
            }
            catch
            {
                return new NotFoundResult();
            }
        }

        [HttpGet("teams_users")]
        public IActionResult GetTeamsWithUsersOlder10()
        {
            try
            {
                return new JsonResult(_customService.GetTeamsWithUsersOlder10().ToArray());
            }
            catch
            {
                return new NotFoundResult();
            }
        }

        [HttpGet("users_task")]
        public IActionResult GetUsersWithTasks()
        {
            try
            {
                return new JsonResult(JsonSerializer.Serialize(_customService.GetUsersWithTasks().ToArray()));
            }
            catch
            {
                return new NotFoundResult();
            }
        }

        [HttpGet("user_structs/{id}")]
        public IActionResult GetUserStruct(int id)
        {
            try
            {
                return new JsonResult(_customService.GetUserStruct(id));
            }
            catch
            {
                return new NotFoundResult();
            }
        }

        [HttpGet("projects_structs")]
        public IActionResult GetProjectStruct()
        {
            try
            {
                return new JsonResult(_customService.GetProjectStruct());
            }
            catch
            {
                return new NotFoundResult();
            }
        }

        [HttpGet("unfinished_Tasks/{id}")]
        public IActionResult GetUnfinishedTasks(int id)
        {
            try
            {
                var result = _customService.GetUnfinishedTasksByUserId(id);
                if (!result.Any())
                    return StatusCode(StatusCodes.Status204NoContent);

                return new JsonResult(JsonConvert.SerializeObject(result));
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
            catch
            {
                return new NotFoundResult();
            }
        }
    }
}

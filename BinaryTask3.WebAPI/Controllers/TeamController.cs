﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BinaryTask3.BLL.Interfaces;
using BinaryTask3.Common.DTOs;
using Microsoft.AspNetCore.Http;

namespace BinaryTask3.WebAPI.Controllers
{
    [ApiController]
    [Route("api/Teams")]
    public class TeamController : ControllerBase
    {
        private readonly ITeamService _teamService;
        public TeamController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public IEnumerable<TeamDTO> GetTeams()
        {
            return _teamService.GetTeams();
        }

        [HttpGet("{id}")]
        public IActionResult GetTeam(int id)
        {
            try
            {
                return new JsonResult(_teamService.GetTeam(id));
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }

        }

        [HttpPost]
        public IActionResult AddTeam([FromBody] TeamDTO team)
        {
            try
            {
                _teamService.CreateTeam(team);
                return new OkResult();
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status409Conflict, e.Message);
            }

        }

        [HttpDelete("{id}")]
        public IActionResult DeleteTeam(int id)
        {
            try
            {
                _teamService.DeleteTeam(id);
                return new OkResult();
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }

        }

        [HttpPut("{id}")]
        public IActionResult UpdateTeam(int id, [FromBody] TeamDTO team)
        {
            try
            {
                _teamService.UpdateTeam(id, team);
                return new OkResult();
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }

        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BinaryTask3.BLL.Interfaces;
using BinaryTask3.Common.DTOs;
using Microsoft.AspNetCore.Http;

namespace BinaryTask3.WebAPI.Controllers
{
    [ApiController]
    [Route("api/Tasks")]
    public class TaskController : ControllerBase
    {
        private readonly ITaskService _taskService;
        public TaskController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public IEnumerable<TaskDTO> GetTasks()
        {
            return _taskService.GetTasks();
        }

        [HttpGet("{id}")]
        public IActionResult GetTask(int id)
        {
            try
            {
                return new JsonResult(_taskService.GetTask(id));
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }

        }

        [HttpPost]
        public IActionResult AddTeam([FromBody] TaskDTO task)
        {
            try
            {
                _taskService.CreateTask(task);
                return new OkResult();
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status409Conflict, e.Message);
            }

        }

        [HttpDelete("{id}")]
        public IActionResult DeleteTask(int id)
        {
            try
            {
                _taskService.DeleteTask(id);
                return new OkResult();
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }

        }

        [HttpPut("{id}")]
        public IActionResult UpdateTask(int id, [FromBody] TaskDTO task)
        {
            try
            {
                _taskService.UpdateTask(id, task);
                return new OkResult();
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }

        }
    }
}

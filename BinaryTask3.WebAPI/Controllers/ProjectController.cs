﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using BinaryTask3.BLL.Interfaces;
using BinaryTask3.Common.DTOs;
using Microsoft.AspNetCore.Http;

namespace BinaryTask3.WebAPI.Controllers
{
    [ApiController]
    [Route("api/Projects")]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectService _projectService;
        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public IEnumerable<ProjectDTO> GetProjects()
        {
            return _projectService.GetProjects();
        }

        [HttpGet("{id}")]
        public IActionResult GetProject(int id)
        {
            try
            {
                return new JsonResult(_projectService.GetProject(id));
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
            
        }

        [HttpPost]
        public IActionResult AddProject([FromBody] ProjectDTO project)
        {
            try
            {
                _projectService.CreateProject(project);
                return new OkResult();
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status409Conflict, e.Message);
            }
            
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteProject(int id)
        {
            try
            {
                _projectService.DeleteProject(id);
                return new OkResult();
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
            
        }

        [HttpPut("{id}")]
        public IActionResult UpdateProject(int id, [FromBody] ProjectDTO project)
        {
            try
            {
                _projectService.UpdateProject(id, project);
                return new OkResult();
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
            
        }

    }
}

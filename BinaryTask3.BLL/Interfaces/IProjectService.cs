﻿using System.Collections.Generic;
using BinaryTask3.Common.DTOs;

namespace BinaryTask3.BLL.Interfaces
{
    public interface IProjectService
    {
        IEnumerable<ProjectDTO> GetProjects();
        ProjectDTO GetProject(int id);
        void UpdateProject(int id, ProjectDTO project);
        void CreateProject(ProjectDTO project);
        void DeleteProject(int id);
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BinaryTask3.Common.DTOs;
using BinaryTask3.Common.CustomModels;

namespace BinaryTask3.BLL.Interfaces
{
    public interface ICustomService
    {
        public Dictionary<ProjectDTO, int> GetProjectTaskCountByUser(int id);
        public IEnumerable<TaskDTO> GetTasksByUserLess45(int id);
        public IEnumerable<(int, string)> GetTasksByUserDone(int id);
        public Dictionary<int, IEnumerable<UserDTO>> GetTeamsWithUsersOlder10();
        public Dictionary<UserDTO, IEnumerable<TaskDTO>> GetUsersWithTasks();
        public UserStruct GetUserStruct(int id);
        public IEnumerable<ProjectStruct> GetProjectStruct();
        public IEnumerable<TaskDTO> GetUnfinishedTasksByUserId(int id);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BinaryTask3.BLL.Interfaces;
using BinaryTask3.Common.CustomModels;
using BinaryTask3.Common.DTOs;
using BinaryTask3.DAL.Interfaces;

namespace BinaryTask3.BLL.Services
{
    public class CustomService : BaseService, ICustomService
    {
        private IEnumerable<ProjectDTO> _projects;
        private IEnumerable<TaskDTO> _tasks;
        private IEnumerable<TeamDTO> _teams;
        private IEnumerable<UserDTO> _users;

        private IEnumerable<ProjectInfo> _projectInfos;
        public CustomService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
            GetData();
        }

        private IEnumerable<ProjectInfo> GetProjectInfos()
        {
            var result = from p in _projects
                join t in _teams on p.TeamId equals t.Id
                join u in _users on p.AuthorId equals u.Id
                select new ProjectInfo()
                {
                    Id = p.Id,
                    TeamId = p.TeamId,
                    AuthorId = p.AuthorId,
                    Name = p.Name,
                    Description = p.Description,
                    Deadline = p.Deadline,
                    CreatedAt = p.CreatedAt,
                    Team = t,
                    Author = u,
                    Tasks = (from task in _tasks
                        join performer in _users on task.PerformerId equals performer.Id
                        where task.ProjectId == p.Id
                        select new TaskInfo()
                        {
                            Id = task.Id,
                            PerformerId = task.PerformerId,
                            ProjectId = task.ProjectId,
                            Performer = performer,
                            Description = task.Description,
                            CreatedAt = task.CreatedAt,
                            FinishedAt = task.FinishedAt,
                            Name = task.Name,
                            State = task.State
                        })
                };

            return result;
        }

        private void GetData()
        {
            _projects = Mapper.Map<IEnumerable<ProjectDTO>>(UnitOfWork.Projects.Read());
            _tasks = Mapper.Map<IEnumerable<TaskDTO>>(UnitOfWork.Tasks.Read());
            _teams = Mapper.Map<IEnumerable<TeamDTO>>(UnitOfWork.Teams.Read());
            _users = Mapper.Map<IEnumerable<UserDTO>>(UnitOfWork.Users.Read());

            _projectInfos = GetProjectInfos();
        }

        public Dictionary<ProjectDTO, int> GetProjectTaskCountByUser(int id)
        {
            GetData();
            try
            {
                var result = _projectInfos.Where(p => p.Author.Id == id)
                    .ToDictionary(
                        p => _projects.First(pr => pr.Id == p.Id),
                        p => p.Tasks.Count());

                return result;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<TaskDTO> GetTasksByUserLess45(int id)
        {
            GetData();
            try
            {
                var result = _projectInfos.SelectMany(p => p.Tasks)
                    .Where(t =>
                        t.Name != null &&
                        t.Performer.Id == id &&
                        t.Name.Length < 45);
                return result;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<(int, string)> GetTasksByUserDone(int id)
        {
            GetData();
            try
            {
                var result = _projectInfos.SelectMany(p => p.Tasks)
                    .Where(t =>
                        t.Performer.Id == id &&
                        t.FinishedAt.HasValue &&
                        t.FinishedAt.Value.Year == DateTime.Now.Year).Select(t => (t.Id ?? 0, t.Name));
                return result;
            }
            catch
            {
                throw;
            }
        }

        public Dictionary<int, IEnumerable<UserDTO>> GetTeamsWithUsersOlder10()
        {
            GetData();
            try
            {
                var result = _projectInfos.Select(p => p.Team).Distinct()
                    .GroupJoin(
                        _users,
                        team => team.Id,
                        user => user.TeamId,
                        (team, user) => (team.Id, team.Name, user))
                    .Where(item => item.user.All(us => us.BirthDay.Year < DateTime.Now.Year - 10)).ToDictionary(i => i.Id ?? 0, i => i.user);

                return result;
            }
            catch
            {
                throw;
            }
        }

        public Dictionary<UserDTO, IEnumerable<TaskDTO>> GetUsersWithTasks()
        {
            GetData();
            try
            {
                var result = _users
                    .Join(_tasks,
                        u => u.Id,
                        t => t.PerformerId,
                        (u, t) => (u, t))
                    .GroupBy(ut => ut.u)
                    .SelectMany(g => g.OrderByDescending(grp => grp.t.Name?.Length))
                    .GroupBy(gr => gr.u)
                    .OrderBy(g => g.Key.FirstName);

                return result.ToDictionary(gr => gr.Key, gr => gr.Select(i => i.t));
            }
            catch
            {
                throw;
            }
        }

        public UserStruct GetUserStruct(int id)
        {
            GetData();
            try
            {
                var result = _projectInfos.Select(_ => new UserStruct()
                {
                    User = (_users.FirstOrDefault(u => u.Id == id)),
                    LastProject = (_projectInfos.Where(p => p.AuthorId == id)
                        .OrderBy(p => p.CreatedAt)?.Last()),
                    ProjectTaskCount = (_projectInfos.SelectMany(p => p.Tasks)
                        .Where(t => t.ProjectId ==
                                    (_projectInfos.Where(p => p.AuthorId == id)
                                        .OrderBy(p => p.CreatedAt)?.Last()).Id)).Count(),
                    UnfinishedTaskCount = (_projectInfos.SelectMany(p => p.Tasks)
                        .Where(t =>
                            t.Name != null &&
                            t.Performer.Id == id &&
                            t.FinishedAt == null)).Count(),
                    LongestTask = _projectInfos.SelectMany(p => p.Tasks)
                        .Where(t =>
                            t.Name != null &&
                            t.Performer.Id == id).OrderBy(t => (t.FinishedAt ?? DateTime.Now) - t.CreatedAt)?.Last()
                }).FirstOrDefault();

                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public IEnumerable<ProjectStruct> GetProjectStruct()
        {
            GetData();
            try
            {
                var result = _projectInfos.Select(p => new ProjectStruct()
                {
                    Project = p,
                    LongestTask = (_projectInfos.SelectMany(pr => pr.Tasks)
                            .Where(t => t.ProjectId == p.Id))
                        .OrderBy(t => (t.Description ?? "").Length).LastOrDefault(),
                    ShortestTask = (_projectInfos.SelectMany(pr => pr.Tasks)
                            .Where(t => t.ProjectId == p.Id))
                        .OrderBy(t => (t.Name ?? "").Length).FirstOrDefault(),
                    UserCount = (_users
                        .Where(u =>
                            (u.TeamId ?? -1) == p.Team.Id &&
                            ((p.Tasks.Count() < 3) || (p.Description?.Length > 20))
                        )
                        .Distinct().Count())
                });

                return result;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<TaskDTO> GetUnfinishedTasksByUserId(int id)
        {
            GetData();
            try
            {
                if(!_users.Select(u => u.Id).Contains(id))
                    throw new ArgumentException($"User with id: {id} not found");

                var result = _tasks.Where(t => t.PerformerId == id && t.FinishedAt == null);
                return result;
            }
            catch
            {
                throw;
            }
        }
    }
}

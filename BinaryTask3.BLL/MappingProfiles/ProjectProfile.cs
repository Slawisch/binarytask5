﻿using AutoMapper;
using BinaryTask3.Common.DTOs;
using BinaryTask3.DAL.Entities;

namespace BinaryTask3.BLL.MappingProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>()
                .ForMember(dest => dest.Deadline, src => src.MapFrom(x => x.DeadlineDate))
                .ForMember(dest => dest.CreatedAt, src => src.MapFrom(x => x.CreatedAtDate));
            CreateMap<ProjectDTO, Project>()
                .ForMember(dest => dest.DeadlineDate, src => src.MapFrom(x => x.Deadline))
                .ForMember(dest => dest.CreatedAtDate, src => src.MapFrom(x => x.CreatedAt));
        }
    }
}
